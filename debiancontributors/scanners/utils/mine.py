# coding: utf8
# Debian Contributors data mining utilities
#
# Copyright (C) 2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging

log = logging.getLogger(__name__)

__all__ = ["Aggregate"]

class Aggregate(dict):
    """
    Aggregate pairs of (key, val) in a dict { key: (minval, maxval) }
    """
    def add(self, key, val):
        """
        Add a (key, val) pair to the aggregation
        """
        old = self.get(key, None)
        if old is None:
            self[key] = (val, val)
        else:
            self[key] = (min(old[0], val), max(old[1], val))
