# coding: utf8
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from debiancontributors.datamine import DataMine
import unittest

class TestMineBts(unittest.TestCase):
    def test_gitdirs(self):
        """
        Test gitdirs scanner
        """
        mine = DataMine(configstr=
"""
source: test

contribution: correspondant
method: bts
dirs: test/bts_spool/archive/ test/bts_spool/db-h/
url: "http://www.example.com/{name}"
""")
        mine.scan()


if __name__ == '__main__':
    unittest.main()
