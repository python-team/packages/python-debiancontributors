# coding: utf8
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from debiancontributors.types import Contribution, Identifier
import unittest

class TestTypes(unittest.TestCase):
    def test_identifier(self):
        """
        Test Identifier operations
        """
        iem = Identifier("email", "enrico@enricozini.org")
        iem.validate()
        self.assertEqual(iem.type, "email")
        self.assertEqual(iem.id, "enrico@enricozini.org")
        self.assertIsNone(iem.desc)

        ied = Identifier("email", "enrico@enricozini.org", "Enrico Zini")
        ied.validate()
        self.assertEqual(ied.type, "email")
        self.assertEqual(ied.id, "enrico@enricozini.org")
        self.assertEqual(ied.desc, "Enrico Zini")

        ilo = Identifier("login", "enrico")
        ilo.validate()
        self.assertEqual(ilo.type, "login")
        self.assertEqual(ilo.id, "enrico")
        self.assertIsNone(ilo.desc)

        ild = Identifier("login", "enrico", "Enrico Zini")
        ild.validate()
        self.assertEqual(ild.type, "login")
        self.assertEqual(ild.id, "enrico")
        self.assertEqual(ild.desc, "Enrico Zini")

        ifp = Identifier("fpr", "1793D6AB75663E6BF104953A634F4BD1E7AD5568")
        ifp.validate()
        self.assertEqual(ifp.type, "fpr")
        self.assertEqual(ifp.id, "1793D6AB75663E6BF104953A634F4BD1E7AD5568")
        self.assertIsNone(ifp.desc)

        ifd = Identifier("fpr", "1793D6AB75663E6BF104953A634F4BD1E7AD5568", "Enrico Zini")
        ifd.validate()
        self.assertEqual(ifp.type, "fpr")
        self.assertEqual(ifd.type, "fpr")
        self.assertEqual(ifd.id, "1793D6AB75663E6BF104953A634F4BD1E7AD5568")
        self.assertEqual(ifd.desc, "Enrico Zini")

        self.assertEqual(iem, ied)
        self.assertEqual(ilo, ild)
        self.assertEqual(ifp, ifd)
        self.assertNotEqual(iem, ilo)
        self.assertNotEqual(iem, ifp)
        self.assertNotEqual(iem, ild)
        self.assertNotEqual(iem, ifd)
        self.assertNotEqual(ied, ilo)
        self.assertNotEqual(ied, ifp)
        self.assertNotEqual(ied, ild)
        self.assertNotEqual(ied, ifd)

    def test_bad_identifier(self):
        from debiancontributors.parser import Fail
        self.assertRaises(Fail, Identifier("foo", "").validate)
        self.assertRaises(Fail, Identifier(3, "").validate)
        self.assertRaises(Fail, Identifier("login", None).validate)
        self.assertRaises(Fail, Identifier("login", "").validate)
        self.assertRaises(Fail, Identifier("login", "enrico", 3).validate)
        self.assertRaises(Fail, Identifier("email", "enrico").validate)
        self.assertRaises(Fail, Identifier("fpr", "zzz").validate)

    def test_bad_contribution(self):
        from debiancontributors.parser import Fail
        self.assertRaises(Fail, Contribution(None).validate)
        self.assertRaises(Fail, Contribution("foo", 3).validate)
        self.assertRaises(Fail, Contribution("foo", None, 3).validate)
        self.assertRaises(Fail, Contribution("foo", url=3).validate)

    def test_auto(self):
        i = Identifier.create_auto("enrico")
        self.assertEqual(i.type, "login")
        self.assertEqual(i.id, "enrico")
        self.assertIsNone(i.desc)

        i = Identifier.create_auto("Enrico Zini <enrico>")
        self.assertEqual(i.type, "login")
        self.assertEqual(i.id, "enrico")
        self.assertEqual(i.desc, "Enrico Zini")

        i = Identifier.create_auto("enrico@debian.org")
        self.assertEqual(i.type, "email")
        self.assertEqual(i.id, "enrico@debian.org")
        self.assertIsNone(i.desc)

        i = Identifier.create_auto("Enrico Zini <enrico@debian.org>")
        self.assertEqual(i.type, "email")
        self.assertEqual(i.id, "enrico@debian.org")
        self.assertEqual(i.desc, "Enrico Zini")

        i = Identifier.create_auto("1793 D6AB 7566 3E6B F104  953A 634F 4BD1 E7AD 5568")
        self.assertEqual(i.type, "fpr")
        self.assertEqual(i.id, "1793D6AB75663E6BF104953A634F4BD1E7AD5568")
        self.assertIsNone(i.desc)

        i = Identifier.create_auto("Enrico Zini <1793 D6AB 7566 3E6B F104  953A 634F 4BD1 E7AD 5568>")
        self.assertEqual(i.type, "fpr")
        self.assertEqual(i.id, "1793D6AB75663E6BF104953A634F4BD1E7AD5568")
        self.assertEqual(i.desc, "Enrico Zini")

        i = Identifier.create_auto("Enrico Zini, the Mad <enrico>")
        self.assertEqual(i.type, "login")
        self.assertEqual(i.id, "enrico")
        self.assertEqual(i.desc, "Enrico Zini, the Mad")

if __name__ == '__main__':
    unittest.main()
