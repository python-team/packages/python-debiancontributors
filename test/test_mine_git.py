# coding: utf8
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from debiancontributors.datamine import DataMine
import unittest
import os.path

class TestMineGit(unittest.TestCase):
    def test_gitdirs(self):
        """
        Test gitdirs scanner
        """
        mine = DataMine(configstr=
"""
source: test

contribution: committer
method: gitdirs
dirs: "."
url: "http://www.example.com/{name}"
""")
        mine.scan()

    def test_gitlogs(self):
        """
        Test gitlogs scanner
        """
        if not os.path.isdir(".git"):
            raise unittest.SkipTest("no idea where to find my git repo when not run from the git checkout")
        mine = DataMine(configstr=
"""
source: test

contribution: committer
method: gitlogs
dirs: ".git"
url: "http://www.example.com/{{email}}"
""")
        mine.scan()
        emails = set(x.id for x in mine.submission.entries.keys())
        self.assertIn("enrico@enricozini.org", emails)
        self.assertNotIn("enrico", emails)

    def test_gitlogs_authormap(self):
        """
        Test gitlogs scanner
        """
        if not os.path.isdir(".git"):
            raise unittest.SkipTest("no idea where to find my git repo when not run from the git checkout")
        mine = DataMine(configstr=
"""
source: test

contribution: committer
method: gitlogs
dirs: ".git"
author_map:
   enrico@.+  enrico  i
url: "http://www.example.com/{{email}}"
""")
        mine.scan()
        emails = set(x.id for x in mine.submission.entries.keys())
        self.assertNotIn("enrico@enricozini.org", emails)
        self.assertIn("enrico", emails)


if __name__ == '__main__':
    unittest.main()
