# coding: utf8
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import debiancontributors as dc
from datetime import date
import unittest
import json
import os.path

class TestSubmission(unittest.TestCase):
    def testIdentifier(self):
        i = dc.Identifier("login", "enrico")
        self.assertEqual(i.type, "login")
        self.assertEqual(i.id, "enrico")
        self.assertIsNone(i.desc)
        i = dc.Identifier("login", "enrico", "Enrico Zini")
        self.assertEqual(i.type, "login")
        self.assertEqual(i.id, "enrico")
        self.assertEqual(i.desc, "Enrico Zini")

    def testMinimalData(self):
        s = dc.Submission("test")
        s.add_contribution_data(dc.Identifier("login", "enrico"), "upload")
        js = s.to_json(indent=1)
        res = json.loads(js)
        self.assertEqual(res, [{
            "id": [ { "type": "login", "id": "enrico" } ],
            "contributions": [ { "type": "upload" } ],
        }])

    def testFullData(self):
        s = dc.Submission("test")
        s.add_contribution_data(dc.Identifier("login", "enrico"), "upload",
                           begin=date(2013, 5, 1), end=date(2013, 11, 30),
                           url="http://www.example.com")
        js = s.to_json(indent=1)
        res = json.loads(js)
        self.assertEqual(res, [{
            "id": [ { "type": "login", "id": "enrico" } ],
            "contributions": [ { "type": "upload",
                                "begin": "2013-05-01", "end": "2013-11-30",
                                "url": "http://www.example.com" } ],
        }])

    def test_auth_token(self):
        if not os.path.isdir("setup.py"):
            raise unittest.SkipTest("no idea where to find my setup.py when not run from the git checkout")

        s = dc.Submission("test")
        self.assertIsNone(s.auth_token)

        s.set_auth_token("foo")
        self.assertEqual(s.auth_token, "foo")

        mock_auth_token_file = os.path.abspath("setup.py")

        s.set_auth_token("@" + mock_auth_token_file)
        self.assertRegexpMatches(s.auth_token, "setup\(")

        s = dc.Submission("test", auth_token="foo")
        self.assertEqual(s.auth_token, "foo")

        s = dc.Submission("test", auth_token="@" + mock_auth_token_file)
        self.assertRegexpMatches(s.auth_token, r"setup\(")


if __name__ == '__main__':
    unittest.main()
