debiancontributors python module
================================

See https://wiki.debian.org/DebianContributors for technical information about
contributors.debian.org

See https://wiki.debian.org/Teams/FrontDesk/DcSiteDevel for this project's page
in the Debian wiki.

## Computing and posting data using dc-tool

You can describe a data source and how to look for contribution data in a
simple configuration file, then run this to perform data mining and submit data
to the site, all in one go:

	dc-tool --mine myconfigfile --post

Without --post, it prints the results of data mining on standard output: it is
useful to test a data mining configuration.

See DATAMINING.rst and the examples/ directory for documentation and examples
for the data mining configuration.

## Posting data using dc-tool

Assuming you [created a data source in the website][newds] called
`myteam.debian.net` with authentication token 'foobar'.

Assuming you have generated a file `submission.json`, with your submission.

You can post it with `dc-tool` using:

	dc-tool --source myteam.debian.net --auth-token foobar --post submission.json

dc-tool will validate the submission for you before posting it.

You can check the contents of the submission by running `dc-tool` without
any option:

	dc-tool submission.json

## Posting data using python code

Assuming you [created a data source in the website][newds] called
`myteam.debian.net` with authentication token 'foobar'.

	import debiancontributors as dc
	from datetime import date

	# Create a Submission
	s = dc.Submission("myteam.debian.net")

	# Add contribution data to it
	s.add_contribution(
		dc.Identifier("email", "enrico@debian.org"),
		dc.Contribution("shave_yaks", date(2013, 1, 1), date(2013, 12, 23)))

	# Post it to the site
	s.set_auth_token("foobar")
	success, info = s.post()
	if not success:
		import json
		print("submission failed:")
		print(json.dumps(info, indent=1))
	else:
		print("submission succeeded")

## Posting data the way you like

Posting a submission is just a matter of building a JSON data structure and
posting it to the site via HTTP POST. See
[DebianContributors Implementation notes](https://wiki.debian.org/DebianContributors#Implementation_notes)
for details.

[newds]: https://wiki.debian.org/DebianContributors#Creating_a_new_data_source
